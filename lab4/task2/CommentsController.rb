load 'Resource.rb'

class CommentsController
  extend Resource

  def initialize
    @comments = []
  end

  def index
    @comments.each.with_index do |comment, index|
      puts "id:#{index + 1} \"#{comment}\""
    end
  end

  def show
    print "Index of comment: "
    id = gets.to_i

    if id < 1
      puts "wrong id"
      id = gets.to_i
    end

    puts "id:#{id} \"#{@comments[id-1]}\""
  end

  def create
    print "Text of comment: "
    comment = gets.chomp

    @comments << comment

    puts "Comment: #{comment}\nindex: #{@comments.find_index(comment) + 1} "
  end

  def update
    print "Index of comment: "
    id = gets.to_i

    if id < 1
      puts "wrong id"
      id = gets.to_i
    end

    print "Old comment \"#{@comments[id-1]}\", Enter the new comment: "
    new_comment = gets.chomp

    @comments[id-1] = new_comment
    index
  end

  def destroy
    print "Index of comment: "
    id = gets.to_i

    if id < 1
      puts "wrong id"
      id = gets.to_i
    end

    @comments.delete_at(id -1)
  end
end
