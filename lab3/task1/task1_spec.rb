require 'rspec'
require './task1.rb'

RSpec.describe "task1" do
  before {File.write(ACTORS_LIST_PATH, "Kent Trammell\nYura Kozhevnikov \nKevin Richardson\nJason Battersby")}
  
  it "#index"do
    expect{index}.to output("Kent Trammell\nYura Kozhevnikov \nKevin Richardson\nJason Battersby\n").to_stdout
  end

  it "#where" do
    expect(where("Kent Trammell")).to eq(0)
  end

  it "#update" do
    update(0, "Artem Shirma")
    expect{index}.to output("Artem Shirma\nYura Kozhevnikov \nKevin Richardson\nJason Battersby\n").to_stdout
  end

  it "#delete" do
    delete(0)
    expect{index}.to output("Yura Kozhevnikov \nKevin Richardson\nJason Battersby\n").to_stdout
  end

  it "#find" do
    expect{find(2)}.to output("Kevin Richardson\n").to_stdout
  end
end
