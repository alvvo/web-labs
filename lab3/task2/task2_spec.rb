require 'rspec'
require './task2.rb'

RSpec.describe "task2" do
  before(:each) do
    File.write(OUTPUT_FILE_PATH, "")
  end

  it "#witout negative number" do
    allow_any_instance_of(Kernel).to receive(:gets).and_return(20,19,18)
    expect{read_students}.to output("Enter age of student\nEnter age of student\nEnter age of student\nKent Trammell 20\nKevin Richardson 19\nYura Kozhevnikov  18\n").to_stdout
  end

  it "#with negative number" do
    allow_any_instance_of(Kernel).to receive(:gets).and_return(18, 19, -1)
    expect{read_students}.to output("Enter age of student\nEnter age of student\nEnter age of student\nYou entered -1 (exit)\nYura Kozhevnikov  18\nKevin Richardson 19\n").to_stdout
  end

  it "#straightaway negative number" do
    allow_any_instance_of(Kernel).to receive(:gets).and_return(-1)
    expect{read_students}.to output("Enter age of student\nYou entered -1 (exit)\n").to_stdout
  end
end
